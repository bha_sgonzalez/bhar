---
title: '[...]  Tutorials'
author: "Bruce Harris and Associates, Inc."
date: "`r Sys.Date()`"
output: pdf_document
bibliography: packages.bib
description: How to use the [...] Toolbox.
documentclass: book
graphics: yes
linestretch: 1.25
link-citations: yes
classoption: openany
site: bookdown::bookdown_site
biblio-style: apalike
---


```{r include=FALSE}
library(bookdown)
library(rmarkdown)
library(knitr)
library(tinytex)
library(knitr)
knit_hooks$set(crop = hook_pdfcrop)
options(tinytex.verbose = TRUE)
options(knitr.graphics.auto_pdf = TRUE)
knitr::opts_chunk$set(fig.pos = 'H')
# automatically create a bib database for R packages
knitr::write_bib((.packages()), 'packages.bib')

#opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
opts_chunk$set(fig.retina=2,dpi=96,out.extra='')

# bookdown::render_book("index.Rmd", "bookdown::pdf_book")
```

# Toolbox Overview {#overview}
This document gives an overview of how to use the [...] ArcMap Toolbox.

The [...] Toolbox provides numerous Python script tools to [...].

This toolbox provides methods to:

* You can reference the sections of .Rmd files using their tag [like this](#tt)


# Opening the Toolbox
In order to access this toolbox, open ArcCatalog (or the ArcCatalog window within ArcMap). Within the 'Catalog Tree' windowpane in ArcCatalog, click the Folder Connections folder and navigate to the location of the toolbox. You may need to add a folder connection by right clicking 'Folder Connections' and clicking 'Connect to Folder.' Then navigate to the folder with the toolbox within the newly connected folder. Then, you can click the plus sign by the toolbox to view the tools within it (Fig. \@ref(fig:defaultImage)).

```{r defaultImage, cache=FALSE,echo=FALSE,message=FALSE,error=FALSE,warning=FALSE,fig.cap="Opening the Create Site Data tool",fig.align='center'}
knitr::include_graphics("figures/defaultImage.png",auto_pdf = TRUE)
```

\pagebreak
\printindex
